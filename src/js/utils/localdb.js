export default function localDb () {
    const ls = window.localStorage;
    const toString = JSON.stringify;
    const toJSON = JSON.parse;
    
    function setCollection (collectionName, collectionArray = []) {
        ls.setItem(collectionName, toString(collectionArray));
        return getCollection(collectionName);
    }
    
    function getCollection (collectionName) {
        if (ls[collectionName]) {
            return toJSON(ls[collectionName]);    
        }
        
        return undefined;
    }
    
    function addItem (collectionName, item) {
        let collection = toJSON(ls[collectionName]);
        collection.push(item);
        setCollection(collectionName, collection);
    }
    
    function getItem (collectionName, itemID) {
        let collection = toJSON(ls[collectionName]);
        return collection.filter(item => item._id === itemID)[0];
    }
    
    function removeItem (collectionName, itemID) {
        let collection = toJSON(ls[collectionName]);
        let newCollectionArray = collection.filter(item => item._id !== itemID);
        setCollection(collectionName, newCollectionArray);
    }
    
    function updateItem (collectionName, itemID, newDataObj) {
        let collection = toJSON(ls[collectionName]);
        let updatedCollection = collection.map(item => {
            if (item._id === itemID) {
                return Object.assign(item, newDataObj);
            }
            
            return item;
        });
        
        setCollection(collectionName, updatedCollection);
    }
    
    return {
        setCollection: setCollection,
        getCollection: getCollection,
        addItem: addItem,
        getItem: getItem,
        removeItem: removeItem,
        updateItem: updateItem
    }
}