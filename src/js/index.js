import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, hashHistory } from 'react-router'
import Home from './views/home';
import Editor from './views/editor';
import SinglePost from './views/single-post';

ReactDOM.render((
    <Router history={ hashHistory }>
        <Route path="/" component={ Home } />
        <Route path="/posts/:postID" component={ SinglePost } />
        <Route path="/editor/:mode/:postID" component={ Editor } />
    </Router>
), document.querySelector('.page-wrapper'));