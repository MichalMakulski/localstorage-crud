import React from 'react';
import { Link } from 'react-router';
    
export default React.createClass({
    render () {
        return <li className="post" data-uid={this.props.uid}>
            <h2><Link to={'posts/' + this.props.uid/*this.createURLslug(this.props.title)*/}>{this.props.title}</Link></h2>
        </li>
    },
    createURLslug (str) {
        return str.trim().toLowerCase().replace(/ /g, '-');
    }
});