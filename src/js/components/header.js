import React from 'react';

export default (props) => { 
    return <header className="page-header">
        <h1>{props.title}</h1>
    </header>
}