import React from 'react';
import Post from './post-item';
    
export default React.createClass({
    render () {
        let posts = this.buildPosts();
        return <ul className="posts-list">
            {posts}
        </ul>
    },
    buildPosts () {
        return this.props.posts.map((post, idx) => {
            return <Post key={idx} uid={post._id} title={post.postTitle} content={post.postContent} />
        });
    }
});