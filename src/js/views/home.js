import React from 'react';
import { Link } from 'react-router';
import Header from '../components/header';
import PostList from '../components/post-list';
import LocalDb from '../utils/localdb';
    
export default React.createClass({
    getInitialState() {
        return {
            posts: []
        };
    },
    componentWillMount() {
        let db = LocalDb();
        let posts = db.getCollection('posts') || db.setCollection('posts');
        
        this.setState({
            posts: posts
        });
    },
    render () {
        return <div id="home">
            <Header title="Home" />
            <Link to='/editor/add-post/post'>Add post</Link>
            <PostList posts={this.state.posts} />
        </div>
    }
});