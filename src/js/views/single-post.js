import React from 'react';
import { Link } from 'react-router';
import Header from '../components/header';
import LocalDb from '../utils/localdb';
    
export default React.createClass({
    getInitialState () {
        return {
            currentPost: null
        }
    },
    componentWillMount () {
        let db = LocalDb();
        
        this.setState({
            currentPost: db.getItem('posts', this.props.params.postID)
        });
    },
    render () {
        return <div id="single-post">
            <Header title={this.state.currentPost.postTitle} />
            <p>
                <Link className="edit-link" to={'/editor/edit-post/'+this.props.params.postID}>Edit post</Link>
                <Link onClick={this.removePost} className="remove-link" to=''>Remove post</Link>
            </p>
            <div dangerouslySetInnerHTML={{__html: this.state.currentPost.postContent}}></div>
        </div>
    },
    removePost (ev) {
        ev.preventDefault();
        let db = LocalDb();
        
        if (window.confirm('Really?')){
            db.removeItem('posts', this.props.params.postID);
            window.location.hash='/'; 
        }
    }
});