import React from 'react';
import TinyMCE from 'react-tinymce';
import Header from '../components/header';
import LocalDb from '../utils/localdb';

export default React.createClass({
    getInitialState() {
        let editedPost;
        let db = LocalDb();
        if (this.props.params.mode === 'edit-post') {
            editedPost = db.getItem('posts', this.props.params.postID);
        }
        return {
            mode: this.props.params.mode,
            post_id: editedPost ? editedPost._id : '', 
            postTitle: editedPost ? editedPost.postTitle : '',
            postContent: editedPost ? editedPost.postContent : ''
        };
    },
    render () {
        let action = this.state.mode === 'add-post' ? 'Add' : 'Update';
        return <div id="editor">
            <Header title="Editor" />
            <form action="" onSubmit={this.handleSubmit}>
                <fieldset>
                    <label htmlFor="post-title">Post title</label>
                    <input onChange={this.updateInput} id="postTitle" type="text" value={this.state.postTitle} placeholder="" />
                </fieldset>
                <fieldset>
                    <label htmlFor="post-content">Post content</label>
                    <TinyMCE 
                        onChange={this.updateInput} 
                        id="postContent" 
                        content={this.state.postContent}
                        config={{
                          plugins: 'link image code',
                          toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                        }}
                    />
                </fieldset>
                <button type="submit" className="button-default">{action} post</button>
            </form>    
        </div>
    },
    updateInput (ev) {
        let value = ev.target.id === 'postTitle' ? ev.target.value : ev.target.getContent();
        let inputField = ev.target.id;
        this.setState({
           [inputField]: value 
        });
    },
    handleSubmit (ev) {
        ev.preventDefault();
        let db = LocalDb();
        let post = {
            _id: this.state.post_id ? this.state.post_id : new Date().toISOString(),
            postTitle: this.state.postTitle,
            postContent: this.state.postContent
        }
        
        if (this.state.mode === 'add-post') {
            db.addItem('posts', post);
            window.location.hash='/';
        } else {
            db.updateItem('posts', this.state.post_id, post);
            window.location.hash='/posts/'+this.state.post_id;
        }
    }
});